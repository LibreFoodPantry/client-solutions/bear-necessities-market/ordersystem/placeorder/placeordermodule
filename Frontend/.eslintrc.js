module.exports = {
    env: {
        browser: true,
        es2021: true
    },
    parserOptions: {
        ecmaVersion: 12,
        sourceType: 'module'
    },
    rules: {
        semi: 'error',
        indent: 'error',
        'no-multi-spaces': 'error',
        'space-in-parens': 'error',
        'no-multiple-empty-lines': 'error',
        'prefer-const': 'error',
        'no-use-before-define': 'error',
        'no-trailing-spaces': 'error',
        'eol-last': 'error'
    }
};


