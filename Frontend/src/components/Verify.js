function checkbox(){
    let checker = false;
    if(($('input[id="cerealCheckbox"]:checked').val()) === 'on' ||
    ($('input[id="pastaCheckbox"]:checked').val()) === 'on' ||
    ($('input[id="pbCheckbox"]:checked').val()) === 'on' ||
    ($('input[id="tunaCheckbox"]:checked').val()) === 'on' ||
    ($('input[id="soupCheckbox"]:checked').val()) === 'on' ||
    ($('input[id="vegCheckbox"]:checked').val()) === 'on' ||
    ($('input[id="fruitCheckbox"]:checked').val()) === 'on'){
        checker = true;
        return checker;
    }
};

$(document).ready(function() {
    $('.emailTextbox').on('input', function(){
        if ($("#emailVerify1").val() != ($("#emailVerify2").val())){
            $("#submitButton").prop('disabled', true);
        }
        if (($(".emailTextbox").val().trim().length) < 1) {
            $(".submitButton").prop('disabled', true);
        } else{
            if ($("#emailVerify1").val() == ($("#emailVerify2").val()) && checkbox()){
                $("#submitButton").prop('disabled', false);
            } else{
                $("#submitButton").prop('disabled', true);
            }
        }
    });
});
