$(document).ready(function () {
    //disable all checkboxes as page loads:
    $('.foodcheckbox').prop('disabled', true);

    //enable / disable all checkboxes depending on order type
    $('input[type=radio][name="orderTypeRadio"]').change(function () {
        if ($('input[id="customRadio"]:checked').val() == 'on') {
            $('.foodcheckbox').prop('disabled', false);
        } else {
            $('.foodcheckbox').prop('disabled', true);
        }
    });
});
