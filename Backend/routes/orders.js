/*
    Filename:        orders.js
    Description:     API routes
    API Homepage:    http://localhost:4000/api
    Default API URI: http://localhost:4000/api/orders
    Notes:
        This file is being imported to server.js
        For developer documentation refer to README.md
*/

const express = require('express');
const router = express.Router();

//Importing Models
const Order = require('../models/Order');

//-x-x-x-x-x POST Request(s) x-x-x-x-x-

/**
 * POST request to add a Order
 * Status: 201 - The order has been saved successfully
 *         204 - An error has occured saving the order
*/
router.post('/post', async (req, res) => {
    const order = new Order({
        orderType: req.body.orderType,
        itemsOrdered: req.body.itemsOrdered,
        dietaryRestrictions: req.body.dietaryRestrictions,
        commentsPreferences: req.body.commentsPreferences,
        email: req.body.email,
        emailVerified: req.body.emailVerified
    });
    try{
        const savedOrder = await order.save();
        res.status(201).json(savedOrder);
    }catch(err){
        res.status(204).json({message: err});
    }
});

//-x-x-x-x-x GET Request(s) x-x-x-x-x-

/**
 * GET request to retrieve ALL orders
 * Status: 200 - The orders have been returned
 *         404 - There are no orders
*/
router.get('/get', async (req, res) => {
    try{
        const orders = await Order.find();
        res.status(200).json(orders);
    }catch(err){
        res.status(404).json({message:err});
    }
});

/**
 * GET request to retrieve a SPECIFIC order given by orderID
 * Parameter: /OrderID
 * Status: 200 - The order has been returned
 *         404 - No order with the given orderID
*/
router.get('/get/:orderID', async (req, res) => {
    try{
        const specificorder = await Order.findById(req.params.orderID);
        res.status(200).json(specificorder);
    }catch(err){
        res.status(404).json({message:err});
    }
});

/**
 * GET request to retrieve ALL orders ordered by a given email address
 * Parameter: /email/emailID
 * Status: 200 - The orders have been returned
 *         404 - There are no orders in the database placed by the given emailID
*/
router.get('/get/email/:emailID', async (req, res) => {
    try{
        const orders = await Order.find({"email": req.params.emailID.toLowerCase()});
        res.status(200).json(orders);
    }catch(err){
        res.status(404).json({message:err});
    }
});

/**
 * GET request to retrieve ALL orders placed on a date and time
 * Parameter: /date/orderDate
 * Status: 200 - The orders have been returned
 *         404 - There are no orders in the database with given date and time
*/
router.get('/get/date/:orderDate', async (req, res) => {
    try{
        const orders = await Order.find({"dateOrdered": req.params.orderDate});
        res.status(200).json(orders);
    }catch(err){
        res.status(404).json({message:err});
    }
});

//-x-x-x-x-x DELETE Request(s) x-x-x-x-x-

/**
 * DELETE request to delete a SPECIFIC order
 * Parameters: /delete/orderID
 * Status: 200 - The order has been deleted
 *         404 - There are no orders in the database with given orderID
*/
router.delete('/delete/:orderID', async (req, res) => {
    try{
        const specificorder = await Order.findByIdAndDelete(req.params.orderID);
        res.status(200).json(specificorder);
    }catch(err){
        res.status(404).json({message:err});
    }
});

//Exporting the routes
module.exports = router;
