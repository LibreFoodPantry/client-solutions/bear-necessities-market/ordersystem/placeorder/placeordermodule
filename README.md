<div id="top"> 
  <h1>Place Order</h1>
</div>

<p>
  <a href="#dart-about">About</a> &#xa0; | &#xa0; 
  <a href="#rocket-technologies">Technologies</a> &#xa0; | &#xa0;
  <a href="#white_check_mark-requirements">Requirements</a> &#xa0; | &#xa0;
  <a href="#checkered_flag-starting-with-docker">Starting</a> &#xa0; | &#xa0;
  <a href="#memo-license">License</a> &#xa0;
</p>

<br>

## :dart: About ##

This Repository holds the PlaceOrder module for the Bear-Necessities-Market site. This is where all of the "production" code lives.

## :rocket: Technologies ##

The following tools were used in this project:

- [Node.js](https://nodejs.org/en/) - A JavaScript runtime environment, this is the basis for servers on this project.
- [CSS](https://en.wikipedia.org/wiki/CSS) - A styling language used with React to develop the appearance of the UI.
- [Jest](https://jestjs.io/) - Testing framework for JavaScript. Used for Frontend and back-end testing.
- [Docker](https://www.docker.com/) - Allows you to containerize your project so your tech stack doesn't affect any other projects.
- [jQuery](https://jquery.com/) - A JavaScript library for HTML document traversal and manipulation, event handling and animations.
- [Express](https://expressjs.com/) - A Node.js web app framework. Used in this project to implement the API.
- [MongoDB](https://www.mongodb.com/) - NoSQL database that uses a JSON format to store objects.
- [Mongoose](https://mongoosejs.com/) - manages relationships between data, provides schema validation, and is used to translate between objects in code and the representation of those objects in MongoDB
- [JavaScript](https://www.javascript.com/) - Primary language utilized for Frontend and back-end development.

## :white_check_mark: Requirements ##

Before starting :checkered_flag:, you need to have [Git](https://git-scm.com) and [Docker](https://www.docker.com/) installed.

## :checkered_flag: Starting (with Docker) ##

```bash
# Clone this project
$ git clone https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/ordersystem/placeorder/placeordermodule.git

# Access
$ cd placeordermodule

# Run the project
$ docker-compose up

# The Frontend server will initialize in the <http://localhost:3000>
# The Brontend server will initialize in the <http://localhost:4000/api>
```

## :memo: License ##

This project is under GNU General Public License. For more details, see the [LICENSE](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/ordersystem/placeorder/plan/-/blob/master/LICENSE.md) file. 

&#xa0;

Made with :heart: by **Place Order Team**

&#xa0;

<a href="#top">Back to top</a>
